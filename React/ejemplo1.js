import React, {useState} /*esto es para el uso de estados*/ from 'react'; //Importo la librería de React
//React es una librería

const Product = () => {
	
//Declara una nueva variable de estados llamada cantidad 
//una variable de estado es un state - que varía dentro del navegador
const [quantity, setQuantity] =useState(0);
	
	const greenButton = { //declaro el estilo style
		backgroundColor: "yellow",
		color: "red",
		padding: "5px 15px",
	
	}
	
	
	const buy = () => { //Declaro una función onclick para el boton
		alert("You selected this product")
		setQuantity(quantity + 1);
	}
	

	return (
	<>
	<h1 style={{color:"red"}}> This is a Product </h1>
	<button style={greenButton} onClick={buy}> Buy </button>
	<h3>Quantity: {quantity}</h3>
	</>
	
	)
	
	
	}
	
	export default Product;
	
	//para añadir estilos de un archivo externo, declaro el css en el otro archivo
	//en el archivo css declaro clases como .title o .button
	// luego en el archivo .js declaro "className=".title""
	//automaticamente anda y aplica el estilo
	
	//Esto es porque tengo otro archivo "index.js" donde tengo
	import React, {Component} from 'react';
	import ReactDOM from 'react-dom';
	
	import Product from './Product';
	import './style.css';  // --------aquí está el estilo
	
	ReactDOM.render(<Product/>, document.getElementById('root'));
	
	// Entonces: archivos indispensables para un proyecto en React son:
	/*
	index.js
	package.json
	Product.js
	style.css
	*/