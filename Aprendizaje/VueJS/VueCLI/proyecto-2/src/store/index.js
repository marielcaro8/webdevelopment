import { createStore } from 'vuex'

export default createStore({
    state: {
        frutas: [
            { nombre: 'Manzana', cantidad: 0 },
            { nombre: 'Chirimoya', cantidad: 0 },
            { nombre: 'Granada', cantidad: 0 }
        ]
    },
    mutations: {
        aumentar(state, index) {
            state.frutas[index].cantidad++
        },
        reiniciar(state) {

            state.frutas.forEach(elemento => {
                elemento.cantidad = 0
            })
        }
    },
    actions: {

    },
    modules: {}
})