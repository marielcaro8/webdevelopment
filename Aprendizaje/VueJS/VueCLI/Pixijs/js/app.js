"use strict";

let app;
var coords = [];
//cargo la aplicación PIXI que crea un STAGE que es el contenedor 
//principal de toda aplicación con PIXIjs
window.onload = function() {
    app = new PIXI.Application({
        width: window.innerWidth,
        height: window.innerHeight,
        backgroundColor: 0xFFFFFF

    });

    //La agrego al documento HTML(se agrega como un canvas)
    document.body.appendChild(app.view);


    //Para que sea resizable y ocupe toda la pantalla
    app.renderer.view.style.position = "absolute";
    app.renderer.view.style.display = "block";

    app.renderer.autoResize = true;
    app.renderer.resize(window.innerWidth, window.innerHeight);

    //Para que dibuje el grid de fondo
    for (var x = 0; x <= app.view.width; x = x + 15) {
        let line = new PIXI.Graphics();
        line.lineStyle(1, 0xF2F2F2, 1);
        line.moveTo(x, 0);
        line.lineTo(x, app.view.height);

        app.stage.addChild(line);


    }
    for (var y = 0; y <= app.view.height; y = y + 15) {
        let line = new PIXI.Graphics();
        line.lineStyle(1, 0xF2F2F2, 1);
        line.moveTo(0, y);
        line.lineTo(app.view.width, y);

        app.stage.addChild(line);

    }

    //Para que escuche el evento click dentro del canvas (app.view) generado por PIXIJS
    app.view.addEventListener("click", function(event) {
        console.log("click");
        getMousePosition(app.view, event);
    });


}


//Esto es para que se escale en todo el ancho y alto de la pantalla
window.addEventListener("resize", function(event) {
    app.renderer.resize(window.innerWidth, window.innerHeight);
    //scaleToWindow(app.renderer.view); //--> Esta funcion sirve para escalar landscape o portrait segun la pantalla

    //Para que redibuje el grid de fondo
    for (var x = 0; x <= app.view.width; x = x + 15) {
        let line = new PIXI.Graphics();
        line.lineStyle(1, 0xF2F2F2, 1);
        line.moveTo(x, 0);
        line.lineTo(x, app.view.height);

        app.stage.addChild(line);


    }
    for (var y = 0; y <= app.view.height; y = y + 15) {
        let line = new PIXI.Graphics();
        line.lineStyle(1, 0xF2F2F2, 1);
        line.moveTo(0, y);
        line.lineTo(app.view.width, y);

        app.stage.addChild(line);

    }
});




//Dibujar una línea rosa
/*
function getMousePosition(canvas, event) {
    let rect = canvas.getBoundingClientRect();
    let x = event.clientX - rect.left;
    let y = event.clientY - rect.top;
    console.log("Coordinate x: " + x,
        "Coordinate y: " + y);
    var coord = { "x": event.x, "y": event.y };

    coords.push(coord);
    var max = coords.length - 1;
    if (typeof coords[max - 1] !== "undefined") {
        var curr = coords[max],
            prev = coords[max - 1];
        let line = new PIXI.Graphics();
        line.lineStyle(4, 0xF2A2F2, 1);
        line.moveTo(prev.x, prev.y);
        line.lineTo(curr.x, curr.y);
        line.interactive = true;
        console.log(line.interactive);
        app.stage.addChild(line);
    }

}*/


function getMousePosition(canvas, event) {

    let rect = canvas.getBoundingClientRect();
    let ax = event.clientX - rect.left;
    let ay = event.clientY - rect.top;
    console.log("Coordinate x: " + event.x,
        "Coordinate y: " + event.y);
    var coord = { "ax": ax, "ay": ay };

    coords.push(coord);
    var max = coords.length - 1;

    //Dibuja un punto circular
    if (typeof coords[max - 1] !== "undefined") {
        var curr = coords[max],
            prev = coords[max - 1];
        let circle = new PIXI.Graphics();
        circle.beginFill(0x9966FF);
        circle.drawCircle(ax, ay, 12);
        circle.endFill();

        app.stage.addChild(circle);

        /*MEJORAR PARA TRATAR DE DIBUJAR UNA LINEA PREVIA ANTES DE DEFINIR LA PARED
	    app.view.addEventListener("mousemove", function(event) {
            console.log("move");
            let rect = canvas.getBoundingClientRect();
            let bx = event.clientX - rect.left;
            let by = event.clientY - rect.top;
            console.log("Coordinate x: " + bx,
                "Coordinate y: " + by);
            var coord = { "bx": bx, "by": by };

            console.log(coords);
            coords.push(coord);
            var max = coords.length - 1;

            if (typeof coords[max - 3] !== "undefined") {
                var curr = coords[max],
                    prev = coords[0];
                let line = new PIXI.Graphics();
                line.lineStyle(4, 0x9966FF, 1);
                console.log(prev);
                console.log(curr);
                line.moveTo(prev.ax, prev.ay);
                line.lineTo(curr.bx, curr.by);
                line.interactive = true;
                console.log(line.interactive);
                app.stage.addChild(line);
            }

            event.stopPropagation();
        });*/
        app.view.addEventListener("mousemove", dibujarIndicador(event, app.view));


    }


}



function dibujarIndicador(event, canvas) {
    console.log("move");
    let rect = canvas.getBoundingClientRect();
    let bx = event.clientX - rect.left;
    let by = event.clientY - rect.top;
    console.log("Coordinate x: " + bx,
        "Coordinate y: " + by);
    var coord = { "bx": bx, "by": by };

    console.log(coords);
    coords.push(coord);
    var max = coords.length - 1;

    if (typeof coords[max - 3] !== "undefined") {
        var curr = coords[max],
            prev = coords[max - 3];
        let line = new PIXI.Graphics();
        line.lineStyle(4, 0x9966FF, 1);
        console.log(prev);
        console.log(curr);
        line.moveTo(prev.ax, prev.ay);
        line.lineTo(curr.bx, curr.by);
        line.interactive = true;
        console.log(line.interactive);
        app.stage.addChild(line);
    }





}