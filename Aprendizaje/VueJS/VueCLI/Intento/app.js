"use strict";




var id;
$(window).resize(function() {

    var canvas = document.getElementById("canva");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    clearTimeout(id);
    id = setTimeout(draw(), 500);
});

function draw() {
    var canvas = document.getElementById("canva");
    var ctx = canvas.getContext("2d");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    ctx.lineWidth = "0.1";
    ctx.strokeStyle = "#000";
    for (var x = 0; x <= canvas.width; x = x + 10) {
        ctx.beginPath();
        ctx.moveTo(x, 0);
        ctx.lineTo(x, canvas.height);
        ctx.stroke();
    }
    for (var y = 0; y <= canvas.height; y = y + 10) {
        ctx.beginPath();
        ctx.moveTo(0, y);
        ctx.lineTo(canvas.width, y);
        ctx.stroke();

    }

    function getMousePosition(canvas, event) {
        let rect = canvas.getBoundingClientRect();
        let x = event.clientX - rect.left;
        let y = event.clientY - rect.top;
        console.log("Coordinate x: " + x,
            "Coordinate y: " + y);

    }

    let canvasElem = document.querySelector("canvas");

    canvasElem.addEventListener("mousedown", function(e) {
        getMousePosition(canvasElem, e);
    });

    var coords = [];
    var canvas = document.getElementById('canva');
    var context = canvas.getContext("2d");
    context.lineWidth = "20";
    context.fillStyle = "#fff";
    context.lineJoin = "round";
    context.lineCap = "round";
    canvas.addEventListener('click', function(event) {
        var coord = { "x": event.x, "y": event.y };
        document.getElementById("coords").innerText = "{" + coord.x + ", " + coord.y + "}";
        coords.push(coord);
        var max = coords.length - 1;
        if (typeof coords[max - 1] !== "undefined") {
            var curr = coords[max],
                prev = coords[max - 1];
            context.beginPath();
            context.moveTo(prev.x, prev.y);
            context.lineTo(curr.x, curr.y);
            context.stroke();
        }
    });

}