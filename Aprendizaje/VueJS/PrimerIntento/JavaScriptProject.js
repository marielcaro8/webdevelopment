const app = new Vue({
    el: '#app',
    data: {
        titulo: 'Hola mundo con Vue',
        frutas: ['manzana', 'pera', 'sandia'],
        nuevafruta: '',
        animales: [
            { nombre: 'Lobo', phylum: 'mamífero' },
            { nombre: 'Delfin', phylum: 'mamífero' },
            { nombre: 'Búho', phylum: 'ave' }
        ],
        nuevoanimal: ''

    },
    methods: {
        agregarfruta() {
            console.log('diste fruta')
            this.frutas.push(
                this.nuevafruta
            );
            this.nuevafruta = ''
        },
        agregaranimal() {
            console.log('diste animal')
            this.animales.push({
                nombre: this.nuevoanimal,
                phylum: '-'
            })
        }
    },

})